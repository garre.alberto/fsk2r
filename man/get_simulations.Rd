% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/getters.R
\name{get_simulations}
\alias{get_simulations}
\title{Returns a summary of the simulations of an FSK object}
\usage{
get_simulations(fsk_object)
}
\description{
Returns a summary of the simulations of an FSK object
}
